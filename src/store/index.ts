import { createStore } from 'vuex';
export default createStore({
  state: {
    current: 1,
  },
  mutations: {
    SET_CURRENT: (store, val) => {
      store.current = val;
    },
  },
  actions: {},
  modules: {
    test: {
      namespaced: true,

      state: {
        hello: '',
      },
      mutations: {
        SET_HELLO: (store, val) => {
          store.hello = val;
        },
      },
      actions: {},
    },
  },
});
