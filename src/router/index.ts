import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import TabBar from '../components/TabBar.vue';
export const asyncRoutes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'tabbar',
    component: TabBar,
    children: [
      {
        path: '',
        name: 'index',
        component: () => import('@/views/index/index.vue')
      },
      {
        path: 'network',
        name: 'network',
        component: () => import('@/views/network/index.vue')
      },
      {
        path: 'user',
        name: 'user',
        component: () => import('@/views/user/index.vue')
      }
    ]
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes: asyncRoutes
});

export default router;
