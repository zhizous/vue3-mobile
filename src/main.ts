import { createApp } from 'vue';
import App from './App.vue';
import router from './router/index';
import store from './store/index';
import vant from './plugins/vant';
import http from './utils/http';

const app = createApp(App);
app.use(http).use(router).use(store).use(vant);

app.mount('#app');
